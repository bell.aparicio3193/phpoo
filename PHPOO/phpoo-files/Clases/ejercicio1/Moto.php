<?php

//crea aqui la clase Moto junto con dos propiedades public

class Moto
{
	//Variables
	public $marca;
	public $modelo;
	public $cilindros;

  //Método constructor
  public function __construct($marca, $modelo, $cilindros)
  {
    $this->marca = $marca;
    $this->modelo = $modelo;
    $this->cilindros = $cilindros;
  }
}
  //inicializamos el mensaje que lanzara con vacio
	$mensaje='';
  //crea aqui la instancia o el objeto de la clase Moto
	$Moto1= new Moto('Honda', '12345', 6);


  if ( !empty($_POST)){

 	 // recibe aqui los valores mandados por post y arma el mensaje para front 
	 	//almacenamos el valor mandado por POST en el atributo color
		 $Moto1->marca=$_POST['marca'];
		 $Moto1->modelo=$_POST['modelo'];
		 $Moto1->cilindros=$_POST['cilindros'];
		 //se construye el mensaje que sera lanzado por el servidor
		 //para concatenar se utiliza . (punto)
		 $mensaje = 'Las características de la Moto son: Marca:  '.$_POST['marca'].' Modelo: '.$_POST['modelo'].' Cilindros: '.$_POST['cilindros'];
  } 

?>

