<?php
//Creación de la clase carro
class Carro2{
	//declaracion de propiedades
	public $color;
	public $modelo;
	private $agno;

	//Declaracion de metodo constructor
	public function __construct($color, $modelo, $agno){
		$this->color=$color;
		$this->modelo=$modelo;
		$this->agno=$agno;
	}

	//Declaracion del método verificación
	public function verificacion($agno){
		$this->agno=$agno;
	}
	//Declaración del método mostrarVerificación para mostrarlo en la tabla
	public function mostrarVerificacion(){
		if ($this->agno<1990) {
			echo "No";
		}elseif ($this->agno>=1990 && $this->agno<2010) {
			echo "Revisión";
		}else{
			echo "Si";
		}
	}
}

//creación de instancia a la clase Carro
$Carro1 = new Carro2('Rojo', 'Renaul', 1998);

if (!empty($_POST)){
	$Carro1->color=$_POST['color'];
	$Carro1->modelo=$_POST['modelo'];
	$Carro1->verificacion($_POST['agno']);
}
