<?php
  
  //Declaración de la clase Usuario
  class usuario{
    //declaracion de atributos
		private $nombre;
		private $contrasena;
		
		//Declaracion de metodo constructor
		public function __construct($nombre){
			$this->nombre=$nombre;
			$this->contrasena=substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 4);
		}

		//declaracion del metodo mostrar para armar el mensaje con el nombre y token
		public function generarContrasena(){
			return 'Hola '.$this->nombre.' esta es tu contraseña : '.$this->contrasena;
		}

		//declaracion de metodo destructor
		public function __destruct(){
			//destruye contrasena y la muestra

			$this->contrasena='<p>La contraseña ha sido destruida.</p>';
			echo $this->contrasena;
		}
  }
  $mensaje='';

  if (!empty($_POST)){
    //creacion de objeto de la clase
    $contrasena= new usuario($_POST['nombre']);
    $mensaje=$contrasena->generarContrasena();
  }
?>