<?php
  include_once("transporte.php");

  class moto extends transporte{

    //atributos propios de la clase

    private $numeroLlantas;

    //declaracion de constructor
		public function __construct($nom,$vel,$com,$llan){
			//sobreescritura de constructor de la clase padre
			parent::__construct($nom,$vel,$com);
			$this->numeroLlantas=$llan;
		}

    // sobreescritura de metodo
    public function resumenMoto(){
      $mensaje=parent::crear_ficha();
      $mensaje.='<tr>
        <td>Numero de llantas:</td>
        <td>'. $this->numeroLlantas.'</td>				
      </tr>';
      return $mensaje;
    }
  }
  $mensaje='';

?>