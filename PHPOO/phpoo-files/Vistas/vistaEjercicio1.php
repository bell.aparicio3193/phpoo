<?php
		include_once("../Clases/ejercicio1/Carro.php");
		include_once("../Clases/ejercicio1/Moto.php"); 
?>
<!-- Termina php- Inicia HTML -->
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/bootstrap-grid.css">
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
	<title>
		Indice
	</title>
</head>
<body>
	
	<input type="text" class="form-control" value="<?php  echo $mensajeServidor; echo $mensaje;?>" readonly>

	<!-- aqui puedes insertar el mesaje del servidor para Moto-->

	<!-- <input type="text" class="form-control" value="<?php   ?>" readonly> -->

	<div class="container" style="margin-top: 4em">
	
	<header> <h1>Carro y Moto</h1></header><br>
	<form method="post">
		<div class="form-group row">

			<label class="col-sm-3" for="CajaTexto1">Color del carro:</label>
			<div class="col-sm-3">
				<input class="form-control" type="color" name="color" id="CajaTexto1">
			</div>
			<div class="col-sm-4">
			</div>

			<!-- inserta aqui los inputs para recibir los atributos del objeto-->
			<label class="col-sm-3" for="CajaTexto1">Marca:</label>
			<div class="col-sm-3">
				<input class="form-control" type="text" name="marca" id="CajaTexto2">
			</div>
			<label class="col-sm-1" for="CajaTexto1">Modelo:</label>
			<div class="col-sm-3">
				<input class="form-control" type="text" name="modelo" id="CajaTexto3">
			</div>
			<label class="col-sm-3" for="CajaTexto1">Cilindros:</label>
			<div class="col-sm-3">
				<input class="form-control" type="number" name="cilindros" id="CajaTexto4">
			</div>
						
		</div>
		<button class="btn btn-primary" type="submit" >Enviar</button>
		<a class="btn btn-link offset-md-5 offset-lg-8 offset-3" href="../index.php">Regresar</a>
	</form>

	</div>


</body>
</html>

